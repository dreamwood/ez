package ez

import (
	"fmt"
	"github.com/nsqio/go-nsq"
)

// 文件日志
type MqLogger struct {
	TopicName  string
	NsqPubAddr string
	producer   *nsq.Producer
}

func NewMqLogger(nsqPubAddr string) *MqLogger {
	fl := &MqLogger{}
	fl.NsqPubAddr = nsqPubAddr
	fl.TopicName = "GORM"
	config := nsq.NewConfig()
	producer, err := nsq.NewProducer(nsqPubAddr, config)
	if err != nil {
		return nil
	}
	fl.producer = producer
	return fl
}
func (this *MqLogger) Printf(format string, v ...interface{}) {
	toWrite := fmt.Sprintf(format, v...)
	if this.producer == nil {
		println("NSQLOG连接失败，内容直接打印：", toWrite)
	}
	ch := make(chan *nsq.ProducerTransaction, 1)
	e := this.producer.PublishAsync(this.TopicName, []byte(toWrite), ch)
	if e != nil {
		println(e)
	}
}
