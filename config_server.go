package ez

type serverConfig struct {
	Port int
	Host string
	//附件目录
	AssetDir string
	Logger *Logger
}

var S *serverConfig

func init() {
	S = &serverConfig{
		Host: "127.0.0.1",
		Port: 8899,
		AssetDir: "./assets",
	}
}
