package ez

import "os"

type globals struct {
	//调试模式：
	//系统内的print,log等内容在debug=false的模式下都应该不输出；
	//自定义的日志排除在此规则之外；
	//默认开启
	Debug bool
	//运行环境的根目录
	AppRoot string
}

var GLB *globals

func init() {
	root, _ := os.Getwd()
	GLB = &globals{
		Debug:   true,
		AppRoot: root,
	}
}
func SetDebug(debug bool) {
	GLB.Debug = debug
}
func GetDebug() bool {
	return GLB.Debug
}
