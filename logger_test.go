package ez

import (
	"testing"
	"time"
)

func TestNewFileLogger(t *testing.T) {
	logger := NewFileLogger()
	for i := 0; i < 100000; i++ {
		logger.Printf("this is log %d", time.Now().UnixNano())
	}
}

//var logger *FileLogger
//
//func init() {
//	logger = NewFileLogger()
//	logger.MaxLine = 100000
//	logger.MaxSize = 10 * 1024
//}

func BenchmarkNewFileLogger(b *testing.B) {
	logger := NewFileLogger()
	for i := 0; i < b.N; i++ {
		logger.Printf("this is log %d", time.Now().UnixNano())
	}
}

func BenchmarkConsoleLogger(b *testing.B) {
	logger := NewConsoleLogger()
	for i := 0; i < b.N; i++ {
		logger.Printf("this is log %d", time.Now().UnixNano())
	}
}

func BenchmarkMqLogger(b *testing.B) {
	logger := NewMqLogger("192.168.7.3:4150")
	for i := 0; i < b.N; i++ {
		logger.Printf("this is log %d", time.Now().UnixNano())
	}
}
