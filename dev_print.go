package ez

import (
	"fmt"
	"runtime"
	"sync"
)

var printLock sync.Mutex

func init() {
	printLock = sync.Mutex{}
}

func PrintError(e error, other ...string) bool {
	if e != nil {
		if GLB.Debug {
			printLock.Lock()
			ColorPrintStart(C_Red*16 + C_White)
			_, f, l, _ := runtime.Caller(1)
			println(fmt.Sprintf("%s:%d", f, l))
			if len(other) > 0 {
				println("[ERROR]", other[0])
			}
			print("[ERROR]", e.Error())
			ColorPrintEnd()
			println("")
			printLock.Unlock()
		}
		return true
	} else {
		return false
	}
}
func HasError(e error, other ...string) bool {
	if e != nil {
		if GLB.Debug {
			printLock.Lock()
			ColorPrintStart(C_Red*16 + C_White)
			_, f, l, _ := runtime.Caller(1)
			println(fmt.Sprintf("%s:%d", f, l))
			if len(other) > 0 {
				println("[ERROR]", other[0])
			}
			print("[ERROR]", e.Error())
			ColorPrintEnd()
			println("")
			printLock.Unlock()
		}
		return true
	} else {
		return false
	}
}
func PrintSuccess(other ...string) {
	if GLB.Debug {
		printLock.Lock()
		ColorPrintStart(C_Green*16 + C_Grey)
		_, f, l, _ := runtime.Caller(1)
		println(fmt.Sprintf("%s:%d", f, l))
		print("[OK]")
		for _, s := range other {
			print(s)
			print(" ")
		}
		ColorPrintEnd()
		println("")
		printLock.Unlock()
	}
}

func PrintWarning(other ...string) {
	if GLB.Debug {
		printLock.Lock()
		ColorPrintStart(223)
		_, f, l, _ := runtime.Caller(1)
		println(fmt.Sprintf("%s:%d", f, l))
		print("[WARN]")
		for _, s := range other {
			print(s)
			print(" ")
		}
		ColorPrintEnd()
		println("")
		printLock.Unlock()
	}
}

func PrintInfo(other ...string) {
	if GLB.Debug {
		printLock.Lock()
		_, f, l, _ := runtime.Caller(1)
		println(fmt.Sprintf("%s:%d", f, l))
		if len(other) > 0 {
			println(other[0])
		}
		printLock.Unlock()
	}
}
