package ez

import "sort"

func IsInArray(array []interface{}, find interface{}) int {
	for index, row := range array {
		if row == find {
			return index
		}
	}
	return -1
}

func IsHasSame(slice1 []string, slice2 []string) bool {
	return false
}

func ArrayToSlice(array []interface{}) []interface{} {
	slice := make([]interface{}, len(array))
	for _, item := range array {
		slice = append(slice, item)
	}
	return slice
}

type IntSlice []int

func (s IntSlice) Len() int           { return len(s) }
func (s IntSlice) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s IntSlice) Less(i, j int) bool { return s[i] < s[j] }

func SliceReverse(s Slice) []interface{} {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s.Swap(i, j)
	}
	return s
}

type Slice []interface{}

func (s Slice) Len() int           { return len(s) }
func (s Slice) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s Slice) Less(i, j int) bool { return i < j }

func IsStringInSlice(find string, findIn []string) bool {
	sort.Strings(findIn)
	pos := sort.SearchStrings(findIn, find)
	return !(findIn[pos] == find)
}
