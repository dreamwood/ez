package ez

import (
	"encoding/json"
	"fmt"
	"reflect"
	"runtime"
	"time"
)

func JsonLog(v interface{}) {
	if GLB.Debug {

		var l = make([]bool, 3)
		println("\r\n")
		println("#####JSON_LOGGER [" + fmt.Sprintf("%d", time.Now().UnixNano()) + "]")
		println(GetDateFromTimeYMDHIS(time.Now(), "/", ":", " "))
		println(GetDateYMDHIS("/", ":", " "))
		for index, _ := range l {
			_, file, line, _ := runtime.Caller(2 - index + 1)
			println(fmt.Sprintf("[%d] %s:%d\t", index, file, line))
		}
		switch v.(type) {
		case string:
			println("data type = string")
			println(v.(string))
		case []byte:
			println("data type = []byte", len(v.([]byte)))
			println(string(v.([]byte)))
		default:
			tp := reflect.TypeOf(v)
			fmt.Printf("data type = unknown %s", tp.String())
			fmt.Printf("%v\n", v)
			str, e := json.MarshalIndent(v, "", "\t")
			PrintError(e)
			println(string(str) + "\r\n")
		}
	}
}

func GetLine() int {
	_, _, line, _ := runtime.Caller(1)
	return line
}
func ThisLine() int {
	return GetLine()
}
func PrintLine() {
	if GLB.Debug {
		_, _, line, _ := runtime.Caller(1)
		println(fmt.Sprintf("Line:%d", line))
	}
}
func Debug(v interface{}) {
	if GLB.Debug {
		//for i := 0; i < 10; i++ {
		//	_, file, line, _ := runtime.Caller(i)
		//	fmt.Printf(fmt.Sprintf("%s:%d\n", file, line))
		//}
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf(fmt.Sprintf("%s:%d\n", file, line))
		fmt.Printf("%v\n", v)
	}
}
func Trace(v interface{}, depth int) {
	if GLB.Debug {
		for i := 0; i < depth; i++ {
			_, file, line, _ := runtime.Caller(i)
			fmt.Printf(fmt.Sprintf("%s:%d\n", file, line))
		}
		fmt.Printf("%v\n", v)
	}
}
