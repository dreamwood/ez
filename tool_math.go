package ez

func Pow(x int, n int) int {
	ans := 1
	for n != 0 {
		ans *= x
		n--
	}
	return ans
}
