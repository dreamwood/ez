package ez

import (
	"log"
	"os"
)

// 文件日志
type ConsoleLogger struct {
	Logger *log.Logger
	Prefix string
}

func NewConsoleLogger() *ConsoleLogger {
	fl := &ConsoleLogger{
		Prefix: "[EZ]",
	}
	fl.Logger = log.New(os.Stdout, fl.Prefix, log.LstdFlags)
	return fl
}
func (this *ConsoleLogger) Printf(format string, v ...interface{}) {
	this.Logger.Printf(format, v...)
}
