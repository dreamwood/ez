package ez

import (
	"fmt"
)

const (
	C_Blue      = 1
	C_Green     = 2
	C_BlueLight = 11
	C_White     = 0
	C_Yellow    = 14
	C_Red       = 4
	C_Pink      = 14
	C_Grey      = 15
	C_Purple    = 5
)

//var kernel32 *syscall.LazyDLL
//var SetConsoleTextAttribute *syscall.LazyProc
//var CloseHandle *syscall.LazyProc

func init() {
	//if runtime.GOOS == "windows" {
	//	kernel32 = syscall.NewLazyDLL("kernel32.dll")
	//	SetConsoleTextAttribute = kernel32.NewProc("SetConsoleTextAttribute")
	//	CloseHandle = kernel32.NewProc("CloseHandle")
	//}
}

func ColorPrint(s string, i int) { //设置终端字体颜色 i 取值256 前景色和后景色共16*16
	//SetConsoleTextAttribute.Call(os.Stdout.Fd(), uintptr(i))
	//println(s)
	//SetConsoleTextAttribute.Call(os.Stdout.Fd(), uintptr(7))

}

func ColorPrintStart(i int) { //设置终端字体颜色
	//
	//if runtime.GOOS == "windows" {
	//	SetConsoleTextAttribute.Call(os.Stdout.Fd(), uintptr(i))
	//	//todo 原先引用的方法这里是需要关闭的，是否会引发性能问题暂时不可知
	//}
}
func ColorPrintEnd() { //设置终端字体颜色
	//if runtime.GOOS == "windows" {
	//	SetConsoleTextAttribute.Call(os.Stdout.Fd(), uintptr(7))
	//}
}

func ShowColor() {
	for i := 0; i < 16; i++ {
		for n := 0; n < 16; n++ {
			ColorPrint(fmt.Sprintf("%d", i*16+n), i*16+n)
		}
	}
}
