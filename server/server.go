package server

import (
	"net/http"
)

type ServerHandler struct {
	http.Handler
}

func (this *ServerHandler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	operation:=NewOperation(resp,req)
	defer operation.Timer.Done()

	//添加一些公共的前置操作
	tPrepare := operation.Timer.Add("GlobalPreparer")
	for name, action := range routeHub.Preparer {
		tPrepareItem := operation.Timer.Add(name)
		(*action)(operation)
		tPrepareItem.Done()
		if operation.IsStop {
			return
		}
	}
	tPrepare.Done()

	//业务逻辑处理流程
	Dispatch(operation)

	//添加一些公共的后续操作
	tFinisher := operation.Timer.Add("GlobalFinisher")
	for name, action := range routeHub.Finisher {
		tFinisherItem := operation.Timer.Add(name)
		(*action)(operation)
		tFinisherItem.Done()
		if operation.IsStop {
			return
		}
	}
	tFinisher.Done()

}

func NewServerHandler() *ServerHandler {
	hd := new(ServerHandler)
	return hd
}