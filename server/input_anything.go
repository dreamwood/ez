package server

import (
	"encoding/json"
	"strconv"
)

type InputAnyThing struct {
	Receive interface{}
}

func NewInputAnyThing(v interface{}) *InputAnyThing {
	return &InputAnyThing{v}
}

func (this *InputAnyThing) IsString(def...string) string {
	find,ok:=this.Receive.(string)
	if ok {
		return find
	}else {
		if len(def) == 0 {
			return ""
		}
		return def[0]
	}
}
func (this *InputAnyThing) IsInt(def...int) int {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.Atoi(this.Receive.(string))
		if e != nil {break}
		return transform
	case int:
		return this.Receive.(int)
	case float64:
		return int(this.Receive.(float64))
	default:
		//core.Debug(reflect.TypeOf(this.Receive).Name())
	}

	if len(def) == 0 {
		return 0
	}
	return def[0]
}
func (this *InputAnyThing) IsInt32(def...int32) int32 {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.ParseInt(this.Receive.(string),10,32)
		if e != nil {break}
		return int32(transform)
	case int32:
		return this.Receive.(int32)
	case float64:
		return int32(this.Receive.(float64))
	default:
	}

	if len(def) == 0 {
		return 0
	}
	return def[0]
}
func (this *InputAnyThing) IsInt64(def...int64) int64 {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.ParseInt(this.Receive.(string),10,64)
		if e != nil {break}
		return transform
	case int64:
		return this.Receive.(int64)
	default:
	}

	if len(def) == 0 {
		return 0
	}
	return def[0]
}
func (this *InputAnyThing) IsInt16(def...int16) int16 {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.ParseInt(this.Receive.(string),10,16)
		if e != nil {break}
		return int16(transform)
	case int16:
		return this.Receive.(int16)
	case float64:
		return int16(this.Receive.(float64))
	default:
	}

	if len(def) == 0 {
		return 0
	}
	return def[0]
}

func (this *InputAnyThing) IsInt8(def...int8) int8 {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.ParseInt(this.Receive.(string),10,8)
		if e != nil {break}
		return int8(transform)
	case int8:
		return this.Receive.(int8)
	case float64:
		return int8(this.Receive.(float64))
	default:
	}

	if len(def) == 0 {
		return 0
	}
	return def[0]
}


func (this *InputAnyThing) IsUint(def...uint) uint {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.Atoi(this.Receive.(string))
		if e != nil {break}
		return uint(transform)
	case uint:
		return this.Receive.(uint)
	default:
	}

	if len(def) == 0 {
		return 0
	}
	return def[0]
}
func (this *InputAnyThing) IsUint32(def...uint32) uint32 {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.ParseUint(this.Receive.(string),10,32)
		if e != nil {break}
		return uint32(transform)
	case uint32:
		return this.Receive.(uint32)
	default:
	}

	if len(def) == 0 {
		return 0
	}
	return def[0]
}
func (this *InputAnyThing) IsUint64(def...uint64) uint64 {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.ParseUint(this.Receive.(string),10,64)
		if e != nil {break}
		return transform
	case uint64:
		return this.Receive.(uint64)
	default:
	}

	if len(def) == 0 {
		return 0
	}
	return def[0]
}
func (this *InputAnyThing) IsUint16(def...uint16) uint16 {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.ParseUint(this.Receive.(string),10,16)
		if e != nil {break}
		return uint16(transform)
	case uint16:
		return this.Receive.(uint16)
	default:
	}

	if len(def) == 0 {
		return 0
	}
	return def[0]
}

func (this *InputAnyThing) IsUint8(def...uint8) uint8 {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.ParseUint(this.Receive.(string),10,8)
		if e != nil {break}
		return uint8(transform)
	case uint8:
		return this.Receive.(uint8)
	default:
	}

	if len(def) == 0 {
		return 0
	}
	return def[0]
}

func (this *InputAnyThing) IsFloat32(def...float32) float32 {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.ParseFloat(this.Receive.(string),32)
		if e != nil {break}
		return float32(transform)
	case float32:
		return this.Receive.(float32)
	default:
	}

	if len(def) == 0 {
		return 0
	}
	return def[0]
}
func (this *InputAnyThing) IsFloat64(def...float64) float64 {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.ParseFloat(this.Receive.(string),64)
		if e != nil {break}
		return transform
	case float64:
		return this.Receive.(float64)
	default:
	}

	if len(def) == 0 {
		return 0
	}
	return def[0]
}

func (this *InputAnyThing) IsBool(def...bool) bool {
	switch this.Receive.(type){
	case string:
		transform,e:=strconv.ParseBool(this.Receive.(string))
		if e != nil {break}
		return transform
	case bool:
		return this.Receive.(bool)
	default:
	}

	if len(def) == 0 {
		return false
	}
	return def[0]
}

func (this *InputAnyThing) IsMap() map[string]interface{} {
	switch this.Receive.(type){
	case string:
		tmp:=make(map[string]interface{})
		e:=json.Unmarshal([]byte(this.Receive.(string)),&tmp)
		if e != nil {break}
		return tmp
	case map[string]interface{}:
		return this.Receive.(map[string]interface{})
	default:
	}
	return nil
}




