package server

import (
	"bytes"
	"encoding/json"
	"net/http"
)

type Output struct {
	Response http.ResponseWriter
	Writer   *bytes.Buffer
	Body     []byte
}

func (this *Output) Json(data interface{}) {
	tmp, e := json.Marshal(data)
	if e != nil {
		this.Html(e.Error())
	} else {
		_, e := this.Writer.Write(tmp)
		if e != nil {
			this.Html(e.Error())
		} else {
			//this.Response.WriteHeader(200)
			this.Response.Header().Set("Content-Type", "application/json")
		}
	}
}
func (this *Output) Html(data string) {
	_, e := this.Writer.Write([]byte(data))
	if e != nil {
		this.Html(e.Error())
	} else {
		this.Response.WriteHeader(200)
	}
}
func (this *Output) Byte(data []byte) {
	_, e := this.Writer.Write(data)
	if e != nil {
		this.Html(e.Error())
	} else {
		this.Response.WriteHeader(200)
	}
}

type JsonOut struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func NewJsonOut(p ...interface{}) *JsonOut {
	jsonOut := new(JsonOut)
	if len(p) > 0 {
		jsonOut.Code = p[0].(int)
	}
	if len(p) > 1 {
		jsonOut.Message = p[1].(string)
	}
	if len(p) > 2 {
		jsonOut.Data = p[2]
	}
	return jsonOut
}
