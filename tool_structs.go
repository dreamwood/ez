package ez

// 多选操作Id数组
type Ids struct {
	Ids []int `json:"ids"`
}

func NewIds() *Ids {
	return &Ids{make([]int, 0)}
}
//
//// 列表查询参数
//type HttpQuery struct {
//	Conditions map[string]interface{} `json:"_where"`
//	Order      []string               `json:"_order"`
//	Page       int                    `json:"page"`
//	Limit      int                    `json:"limit"`
//	Depth      int                    `json:"_d"`
//	Group      string                 `json:"_g"`
//	Search     string                 `json:"_search"`
//}
//
//func (this *HttpQuery) GetGroup() string {
//	if this.Group == "" {
//		return "default"
//	}
//	return this.Group
//}
//
//func NewHttpQuery() *HttpQuery {
//	return &HttpQuery{
//		Conditions: make(map[string]interface{}),
//		Order:      make([]string, 0),
//		Page:       1,
//		Limit:      10,
//		Depth:      1,
//	}
//}
//
//type ChoiceMaker struct {
//	Fields  string `json:"fields"`
//	HasNull bool
//}
