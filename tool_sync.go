package ez

import "sync"

// 全局的WaitGroup

type EzWaitGroup struct {
	WaitGroups map[string]*sync.WaitGroup
}

var EzWg *EzWaitGroup

func init() {
	EzWg = &EzWaitGroup{
		WaitGroups: make(map[string]*sync.WaitGroup),
	}
}

func GetWg() *EzWaitGroup {
	return EzWg
}

func (this *EzWaitGroup) CreateWaitGroup(name string) *sync.WaitGroup {
	this.WaitGroups[name] = &sync.WaitGroup{}
	return this.WaitGroups[name]
}

func (this *EzWaitGroup) Get(name string) *sync.WaitGroup {
	return this.WaitGroups[name]
}

func (this *EzWaitGroup) Add(name string) {
	this.AddInt(name, 1)
}
func (this *EzWaitGroup) AddInt(name string, delta int) {
	this.Get(name).Add(delta)
}

func (this *EzWaitGroup) Done(name string) {
	this.Get(name).Done()
}

func (this *EzWaitGroup) Clear(name string) {
	delete(this.WaitGroups, name)
}

//全局的通道

// todo 等用的时候再测试补充
type EzChannel struct {
	Channels map[string]chan int
}

func (this *EzChannel) Create(name string) {

}
