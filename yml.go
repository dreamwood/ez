package ez

import (
	"gopkg.in/yaml.v2"
)

type YmlConfig struct {
	FilePath string
}

func CreateConfigFromYml(path string, node string, model interface{}) {
	yc := new(YmlConfig)
	yc.SetFilePath(path)
	all := make(map[string]interface{})
	yc.FillModel(&all)
	nodeData, ok := all[node]
	if !ok {
		return
	}
	tmp, e := yaml.Marshal(nodeData)
	if e != nil {
		PrintError(e)
		return
	}
	e = yaml.Unmarshal(tmp, model)
	PrintError(e)
}

func (this *YmlConfig) SetFilePath(path string) {
	this.FilePath = path
}

func (this *YmlConfig) FillModel(model interface{}) {
	content := FindAndOpen(this.FilePath)
	if content != nil {
		e := yaml.Unmarshal(content, model)
		PrintError(e)
	}
}
